function sliceText(str, count){
 return str.slice(0, count);
}

function limitedText(elem, count) {
	elem.each(function(){
		var $elem = $(this);		

	    if ($elem.text().length > count){
			$elem.text(sliceText($elem.text(), count)+'...');
		}
	});
}

function goToAnchors(anchorsLink) {
	$.each(anchorsLink, function () {
		var $link = $(this);
		
		$link.on('click', function () {	
			if(window.innerWidth < 886) {
				$('.c-mobile-menu').hide();
			}				
			$('.c-mobile-menu-link').removeClass('active');
			
			var $elem = $(this);
			$elem.addClass('active');
			anchorsLink.not($elem).removeClass('active');
			
			var $anchor = $('[id="' + $elem.attr('href').replace('#', '') + '"]');
			
			if($('body').hasClass('c-cf')) {
				var destination = $anchor.offset().top - 62;
				$('html:not(:animated),body:not(:animated)').animate({scrollTop: destination}, 800);
				return false;
			} else {				
				var destination = $anchor.offset().top - 100;
				$('html:not(:animated),body:not(:animated)').animate({scrollTop: destination}, 800);
				return false;
			}
		});
	});
}

function showSelectedVideo() {
	$('body').on('click', '.c-link-video', function(e) {
		e.preventDefault();
		var src = $(this).data('src') + '?rel=0&amp;autoplay=1'; //autoplay
		var $blVideo = $('.c-video iframe');
		$blVideo.attr('src', src);
		
		$('html, body').stop().animate({ scrollTop: $blVideo.offset().top - 100});		
	});
}

function displayMenuMobile() {
	$('body').on('click', '.c-mobile-menu-link', function(e) {
		e.preventDefault();
		var $link = $(this)
		$link.toggleClass('active');
		
		if($link.hasClass('active')) {
			$('.c-mobile-menu').show();
		} else {
			$('.c-mobile-menu').hide();
		}
		
		$()
	});
}
function scrollWindow(count, anchor) {
	if($(window).scrollTop() + count > anchor.offset().top) {
		var id = anchor.attr('id');
		var anchorLink = $('a[href="#' + id + '"]');
		anchorLink.addClass('active');				
		$('.c-link-anchor').not(anchorLink).removeClass('active');				
	}
}

$(function () { 
	limitedText($('.c-limited-text'), 104);
	goToAnchors($('.c-link-anchor'));
	showSelectedVideo();
	displayMenuMobile();
	
	$(window).scroll(function () {		
		$('.c-anchor').each(function() {
			var anchor = $(this);
			if($('body').hasClass('c-cf')){
				scrollWindow(63, anchor);
			} else {				
				scrollWindow(101, anchor);
			}			
		});		
	});
});
